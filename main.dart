import 'package:flutter/material.dart';
import 'package:nomawethu_jaceni_module_3/registration.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ThemeData(
        primaryColor: Colors.grey, scaffoldBackgroundColor: Colors.amberAccent);
    return const MaterialApp(home: Register());
  }
}
