import 'package:flutter/material.dart';
import 'package:nomawethu_jaceni_module_3/registration.dart';


class About extends StatelessWidget {
  const About({ Key? key }) : super(key: key);

 static const String _title = 'About Page';

  @override
  Widget build(BuildContext context) {
    return Center(
      child: MaterialApp(
        title: _title,
        home: Scaffold(
          backgroundColor: Colors.amberAccent,
          appBar: AppBar(centerTitle: true, title: const Text(_title)),
          body: const Center(child: Text("My Name is Nomawethu Jaceni")
          ),
          
        ),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(
          15,
        ),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 32.0),
                child: Center(
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const Register()));
                    },
                    child: const Text("About Page"),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
